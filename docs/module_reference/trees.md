# serializable_trees.trees

_High-level classes_

**Typical import statement:**

```
>>> from serializable_trees import trees
```

## Module contents

serializable\_trees.trees.**DEFAULT\_INDENT**

> Default indent for JSON and YAML serializations.

> Value: `2`

serializable\_trees.trees.**ScalarType**

> Alias to the serializable\_trees.basics.**[ScalarType](../basics/#module-contents)**
> [Type alias](https://docs.python.org/3/library/typing.html#type-aliases),
> covers **bool**, **float**, **int**, **str** and `None`.

serializable\_trees.trees.**NodeSubclassType**

> [Type alias](https://docs.python.org/3/library/typing.html#type-aliases)
> for both **Node** subclasses’ instances, covers **ListNode** and **MapNode**.

serializable\_trees.trees.**BranchType**

> Alias to the serializable\_trees.basics.**[BranchType](../basics/#module-contents)**
> [Type alias](https://docs.python.org/3/library/typing.html#type-aliases),
> covers **ListNode**, **MapNode**, **bool**, **float**, **int**, **str** and `None`.


## Classes

### TraversalPath

!!! note ""

    serializable\_trees.trees.**TraversalPath**(_\*components: **ScalarType**_)

> Provides a traversal path for nested **Node** instances.
> Path _components_ are **ScalarTypes** used as keys for the **Nodes**.

> **TraversalPath** instances are comparable,
> [hashable](https://docs.python.org/3/glossary.html#term-hashable)
> and immutable objects, and can be used as set items or dict keys.

> - **hash**(TraversalPath\_instance) returns a hash calculated over the string representation
> - **len**(TraversalPath\_instance) returns the number of components
> - **bool**(TraversalPath\_instance) returns `False` on empty paths

#### Instance methods

##### .traverse()

!!! note ""

    **.traverse**(_start: **BranchType**_) → **BranchType**

> Traverse the tree of nested **Node** instances starting at _start_ and return
> the **ScalarType** or **Node** at the end of the path.

##### .partial\_walk()

!!! note ""

    **.partial_walk**(_start: **Node**, fail\_on\_missing\_keys: **bool** = `True`, minimum\_remaining\_components: **int** = `1`_) → **Tuple\[Node, List\[ScalarType\]\]**
    
> Traverse the tree of nested **Node** instances starting at _start_,
> but stop _minimum\_remaining\_components_ before the end of the path,
> and return a **Tuple** containing the **Node** instance at the end point
> and the remaining (ie. not traversed) path components.

> If fail\_on\_missing\_keys is set to `False`,
> do not raise an Exception (**IndexError** or **KeyError** depending
> on the **Node** type) when a key (= path component) is missing,
> but return the last encountered **Node** and the
> remaining path components from there.


### Tree

!!! note ""

    serializable\_trees.trees.**Tree**(_root: **BranchType**_)

> Provides a tree of nested **Node** and **ScalarType** instances.

> **Tree** instances are [mutable](https://docs.python.org/3/glossary.html#term-mutable)
> objects and can be compared to each other
> (internally comparing the roots of both instances).

> **Tree** instances can be cloned using
> [copy.deepcopy()](https://docs.python.org/3/library/copy.html#copy.deepcopy)
> from the standard library, but also have a **.clone()** method
> doing the same.


#### Instance attribute

**.root**

> The **Node** or leaf at the root of the tree 


#### Class methods

##### .from\_file()

!!! note ""

    **.from\_file**(_str\_or\_path: **Union\[str, pathlib.Path\]**, encoding: **str** = `"utf-8"`_) → **Tree**

> Create a new **Tree** instance from a JSON or YAML file.

> Arguments:

> * _str\_or\_path_ → the absolute or relative file name
>   either as **str** or as a
>   [pathlib.Path](https://docs.python.org/3/library/pathlib.html#pathlib.Path)
>   instance.
> * _encoding_ → the file encoding. The default `"utf-8"` should be adequate
>    in most cases.

> Internally, the data structure is deserialized using the **.from\_yaml()**
> class method because it can deserialize JSON as well.

##### .from\_json()

!!! note ""

    **.from\_json**(_json\_representation: **str**_) → **Tree**

> Create a new **Tree** instance from a JSON representation.

##### .from\_yaml()

!!! note ""

    **.from\_yaml**(_yaml\_representation: **str**_) → **Tree**

> Create a new **Tree** instance from a YAML or JSON representation.


#### Instance methods

_Method and argument names are partially botanically inspired (crop, graft, sprout, truncate)._

##### .clone()

!!! note ""

    **.clone**() → **Tree**

> Return a deep copy of the **Tree** object, just like
> [copy.deepcopy()](https://docs.python.org/3/library/copy.html#copy.deepcopy) does.


##### .crop()

!!! note ""

    **.crop**(_path: **TraversalPath**_) → **BranchType**

> Remove the **ScalarType** or **Node** instance determined by _path_
> from the tree, and return it.

> If _path_ is empty and the root is a leaf,
> set the root to an empty **MapNode** instance and return the leaf.


##### .get\_branch\_clone()

!!! note ""

    **.get\_branch\_clone**(_path: **TraversalPath**_) → **BranchType**

> Return a deep copy of the **ScalarType** or **Node** instance determined by _path_.


##### .get\_native\_item()

!!! note ""

    **.get\_native\_item**(_path: **TraversalPath**_) → **Union\[ScalarType, Dict, List\]**

> Return the native data type equivalent of the **ScalarType**
> or **Node** instance determined by _path_.


##### .get\_original\_branch()

!!! note ""

    **.get\_original\_branch**(_path: **TraversalPath**_) → **BranchType**

> Return the original **ScalarType** or **Node** instance determined by _path_.

!!! caution "Handle with care"

    Working with original branches might have unintended side effects.
    Prefer the [get\_branch\_clone()](#get_branch_clone) method whenever possible.


##### .graft()

!!! note ""

    **.graft**(_path: **TraversalPath**, sprout: **BranchType**_) → `None`

> Add _sprout_ to the tree at _path_, replacing former contents at _path_
> if necessary.
> If _path_ does not fully exist in the tree yet, the missing parts will be created.

> Raise a **TypeError** if a leaf exists at an intermediate position.


##### .joined\_tree()

!!! note ""

    **.joined\_tree**(_other: **Tree**, extend\_lists: **bool** = `False`_) → **Tree**

> Return a new **Tree** instance containing joined branches,
> with data from _other_ taking precedence if required.

> Only if _extend\_lists_ is explicitly set `True`,
> **ListNode** instances in the new **Tree** instance will contain all members
> from the **ListNode** instances at the same location in both source trees.
> If the argument ist left at the default (`False`),
> merged **ListNode** instances will only contain members from _other_.

> Please note that the returned **Tree** instance will always contain copies
> instead of the original **Node** and **ScalarType** instances.

!!! tip ""

    This method uses the [basics.merge\_branches()](../basics/#merge_branches)
    function internally.


##### .truncate()

!!! note ""

    **.truncate**(_path: **Optional\[TraversalPath\]**_) → `None`

> Empty the item at _path_. If it is a leaf,
> set it to an empty **MapNode** instance.


##### .to\_json()

!!! note ""

    **.to\_json**(_indent: **Optional\[int\]** = **DEFAULT\_INDENT**, sort\_keys: **bool** = `False`_) → **str**

> Convert the tree (ie. its contents from self.root) to a JSON representation.
> The arguments are passed through to the
> **[json.dumps()](https://docs.python.org/3/library/json.html#json.dumps)** function.


##### .to\_yaml()

!!! note ""

    **.to\_yaml**(_indent: **Optional\[int\]** = **DEFAULT\_INDENT**, sort\_keys: **bool** = `False`_) → **str**

> Convert the tree (ie. its contents from self.root) to a YAML representation.
> The arguments are passed through to the
> **yaml.safe\_dump()** function.

