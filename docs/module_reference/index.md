# serializable_trees

_Base module_

**Typical import statement:**

```
>>> from serializable_trees import TraversalPath, Tree
```

## Module contents

serializable_trees.**TraversalPath**

> Alias to [trees.TraversalPath](./trees/#traversalpath)

serializable_trees.**Tree**

> Alias to [trees.Tree](./trees/#tree)

serializable_trees.**\_\_version\_\_**

> Package version


