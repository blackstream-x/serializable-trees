# serializable_trees.basics

_The basic building blocks used to construct trees_

**Typical import statement:**

```
>>> from serializable_trees import basics
```

## Module contents

serializable\_trees.basics.**ScalarType**

> [Type alias](https://docs.python.org/3/library/typing.html#type-aliases),
> covers **bool**, **float**, **int**, **str** and `None`.

serializable\_trees.basics.**BranchType**

> [Type alias](https://docs.python.org/3/library/typing.html#type-aliases)
> for both **Node** subclasses’ instances as well as all **ScalarType**s:

> covers **ListNode**, **MapNode**, **bool**, **float**, **int**, **str** and `None`.


## Exceptions

### CircularGrowthException

!!! note ""

    serializable\_trees.basics.**CircularGrowthException**(_affected\_instance_)

> Raised if a circular reference is detected in any **Node** instance –
> _affected\_instance_ is used for the exception output.


### ItemTypeInvalid

!!! note ""

    serializable\_trees.basics.**ItemTypeInvalid**(_affected\_instance_)

> Raised if an invalid item is encountered in a **Node**’s collection –
> _affected\_instance_ is used to determine the class name for output.


## Classes

### Node

!!! note ""

    serializable\_trees.basics.**Node**()

> Abstract base class for **ListNodes** and **MapNodes**


#### Instance methods

##### .\_avoid\_circular\_growth()

!!! note ""

    **.\_avoid\_circular\_growth**(_\*ancestors: **Node**_) → `None`

> Prevent infinite recursion loops by recursively checking
> **Node** instances to be added to the internal collection and raising
> a **CircularGrowthException** if a circular reference is detected.


### ListNode

!!! note ""

    serializable\_trees.basics.**ListNode**(_sequence_)

> [List](https://docs.python.org/3/library/stdtypes.html#lists) subclass additionally inheriting the
> [.\_avoid\_circular\_growth()](#_avoid\_circular\_growth) method from
> **[Node](#node)**.

> Two **ListNode** instances compare equal if they have the same number of items
> and all items of both instances are equal.

> **ListNode** instances inherit the entire **list** interface.


### MapNode

!!! note ""

    serializable\_trees.basics.**MapNode**(_mapping: **Optional\[Dict\]** = `None,` \*\*keyword\_arguments_)

> [Dict](https://docs.python.org/3/library/stdtypes.html#mapping-types-dict) subclass inheriting the
> [.\_avoid\_circular\_growth()](#_avoid\_circular\_growth) method from
> **[Node](#node)** and additionally exposing keys that are
> [valid Python identifiers](https://docs.python.org/3/reference/lexical_analysis.html#identifiers)
> as instance "data" attributes.

> If _mapping_ is `None`, the **MapNode** instance is initialized with an empty dict.
> If _keyword\_arguments_ are given, they are added to the internal dict
> (possibly overwritig pre-existing keys with the same name).

> Two **MapNode** instances compare equal if they have the same number of items
> and all (key, value) items of both instances are equal.

> **MapNode** instances inherit the entire **dict** interface.
> Additionally, the following attribute access operations are implemented:

> - attribute setting / updating → > ```map_node_instance.attribute = value``` (uses item assignment internally)
> - attribute retrieval → ```value = map_node_instance.attribute``` (uses item retrieval internally)
> - attribute deletion → ```del map_node_instance.attribute``` (uses item deletion internally)

> When accessing a non-existing item…

> - … through item access (ie. subscript), a **KeyError** is raised
> - … through attribute access, an **AttributeError** is raised (wrapping the internal **KeyError**)


## Module-level functions

### grow\_branch()

!!! note ""

    serializable\_trees.basics.**grow\_branch**(_data\_structure: **Union\[ScalarType, Dict, List\]**_) → **BranchType**

> Return nested **BranchType** instances constructed
> from the provided _data\_structure_.
> Inverse function of **[native\_types()](#native_types)**.


### merge\_branches()

!!! note ""

    serializable\_trees.basics.**merge\_branches**(_branch\_1: **BranchType**, branch\_2: **BranchType**, extend\_lists: **bool** = `False`_) → **BranchType**

> Return a new branch (nested **BranchType** instances)
> where _branch\_2_ was merged into _branch\_1_.

> Only if _extend\_lists_ is explicitly set `True`,
> **ListNode** instances in the new branch will contain all members
> from the **ListNode** instances at the same location in both source branches.
> If the argument is left at the default (`False`),
> merged **ListNode** instances will only contain members from _branch\_2_.

> Please note that the returned branch will always contain copies
> instead of the original **BranchType** instances.


### native\_types()

!!! note ""

    serializable\_trees.basics.**native\_types**(_branch\_root: **BranchType**_) → **Union\[ScalarType, Dict, List\]**

> Return a native data structure constructed from _branch\_root_.
> Inverse function of **[grow\_branch()](#grow_branch)**.
