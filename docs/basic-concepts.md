# Basic concepts

## Data types

### Trees

!!! note ""

    **[→ Tree API reference](../module_reference/trees/#tree)**

A tree consists of a combination of **[Scalars](#scalars)**,
**[ListNodes](#listnodes)** and **[MapNodes](#mapnodes)**.
The term **Branch** is used as a synonym for any of these types.


### Scalars

Native data types **bool**, **float**, **int** and **str**, and `None`.


### ListNodes

!!! note ""

    **[→ ListNode API reference](../module_reference/basics/#listnode)**

[List](https://docs.python.org/3/library/stdtypes.html#lists) subclass
 containing zero or more **Branches**.

**ListNodes** support the following operations:

- ```branch = list_node[index]``` → get the branch at _index_.
- ```list_node[index] = branch``` → store a branch at _index_
     if this index already existed. The branch previously stored there gets lost.
- ```del list_node[index]``` → remove the branch at _index_.
- ```for branches in list_node``` → iterate over the stored branches.
- ```branch in list_node``` → return `True` if the branch _branch_
  exists in _list\_node_.
- ```len(list_node)``` → return the number of branches stored in _list\_node_.
- ```bool(list_node)``` → return `True` if _list\_node_ contains any branches.
- ```list_node_1 == list_node_2```
  → return `True` if _list\_node\_1_ and _list\_node\_2_ contain
  the same branches in the same sequence, `False` otherwise.
- ```list_node_1 != list_node_2```
  → return `True` if _list\_node\_1_ and _list\_node\_2_ contain
  different branches or the same branches in a different sequence,
  `False` otherwise.
- ```list_node.append(branch)``` → store a branch at the end of _list\_node_.
- ```list_node.insert(index, branch)``` → insert a branch at _index_.
- ```list_node.pop(index)``` → remove the branch at _index_, and return it.
- ```list_node_clone = copy.deepcopy(list_node)``` → get a deep copy (clone) of _list\_node_.


### MapNodes

!!! note ""

    **[→ MapNode API reference](../module_reference/basics/#mapnode)**

[Dict](https://docs.python.org/3/library/stdtypes.html#mapping-types-dict) subclasses
with keys restricted to **[Scalars](#scalars)** and values restricted to **Branches**.

In addition to standard library dicts, **MapNode** values can be accessed
as attributes if the corresponding key is a
[valid Python identifier](https://docs.python.org/3/reference/lexical_analysis.html#identifiers).

Through this behaviour, nested *MapNodes* can be accessed
like nested namespaces: ```level_1.level_2.level3``` etc.

**MapNodes** support the following operations:

- ```branch = map_node[key]``` → get the branch stored using _key_ (item access)
- ```map_node[key] = branch``` → store a branch at _key_ (item access).
  If a branch had been stored there previously, it gets lost.
- ```del map_node[key]``` → delete the branch at _key_ (item access)
- ```branch = map_node.key``` → get the branch stored using _key_ (attribute access)
- ```map_node.key = branch``` → store a branch at _key_ (attribute access).
  If a branch had been stored there previously, it gets lost.
- ```del map_node.key``` → delete the branch at _key_ (attribute access).
- ```for key in map_node``` → iterate over the keys.
- ```key in map_node``` → return `True` if the key _key_ exists in _map\_node_.
- ```len(map_node)``` → return the number of (key, branch) items in _map\_node_.
- ```bool(map_node)``` → return `True` if _map\_node_ contains any (key, branch) items.
- ```map_node_1 == map_node_2``` → return `True` if _map\_node\_1_ and _map\_node\_2_.
  contain the same (key, branch) items, `False` otherwise.
- ```map_node_1 != map_node_2``` → return `True` if any (key, branch) items
  in _map\_node\_1_ are different from those in _map\_node\_2_, `False` otherwise.
- ```map_node_clone = copy.deepcopy(map_node)``` → get a deep copy (clone) of _map\_node_.


### TraversalPaths

!!! note ""

    **[→ TraversalPath API reference](../module_reference/trees/#traversalpath)**

Helper class for some **[Tree](#trees)** methods.

Can be used to traverse through **Branches** directly,
but is mainly used as argument for **[Tree](#trees)** instance methods.
