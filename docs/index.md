# Introduction

_Easy-to-use serializable tree datatypes for Python_

## Installation

!!! note ""

    Using a [virtual environment](https://packaging.python.org/en/latest/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment)
    is strongly recommended.


You can install the [package from PyPI](https://pypi.org/project/serializable-trees/):

```bash
pip install --upgrade serializable-trees
```


## Notable changes

### v0.5.0

**ListNode** is a subclass of **list**,
and **MapNode** is a subclass of **dict**.


### v0.4.0

!!! danger "Possible infinite loops with releases prior to 0.4.0"

    In early releases, **ListNode** and MapNode contents were not
    checked for circular references, which could lead to infinite loops.

    Starting with release 0.4.0, all **Node** subclass instances
    implement additional checks that prevent the creation of
    circular node references.


## Package contents

The **serializable_trees** package contains the modules

- **serializable\_trees.basics**
  → [Low-level API](module_reference/basics/): **[ListNode](basic-concepts/#listnodes)**
  and **[MapNode](basic-concepts/#mapnodes)** classes
  used as internal building blocks for Trees, and helper functions
- **serializable\_trees.trees**
  [→ High-level API](module_reference/trees/): classes
  **[TraversalPath](basic-concepts/#traversalpaths)**
  and **[Tree](basic-concepts/#trees)**

Both high-level API classes from the **[trees](module_reference/trees/)** module
are aliased in the [base module](module_reference/) so they
can simply be imported using

```
>>> from serializable_trees import TraversalPath, Tree
```
