# Glossary

## Branch

Any combination of **[Scalar](#scalar)**s, nested **ListNode** and **MapNode** instances
– represented by the [Type alias](https://docs.python.org/3/library/typing.html#type-aliases)
**[BranchType](#branchtype)**.

## BranchType

[Type alias](https://docs.python.org/3/library/typing.html#type-aliases) for **[Branch](#branch)**es.

## Leaf

An alias to **[Scalar](#scalar)**, used in the documentation and some error messages.

## ListNode

The [serializable\_trees.basics.ListNode](../module_reference/basics/#listnode) class.

## MapNode

The [serializable\_trees.basics.MapNode](../module_reference/basics/#mapnode) class.

## Node

The abstract [serializable\_trees.basics.Node](../module_reference/basics/#node) class.

## Scalar

A **bool**, **float**, **int** or **str** instance, or `None`
– represented by the [Type alias](https://docs.python.org/3/library/typing.html#type-aliases)
**[ScalarType](#scalartype)**.

## ScalarType

[Type alias](https://docs.python.org/3/library/typing.html#type-aliases) for **[Scalar](#scalar)**s.

## TraversalPath

The [serializable\_trees.trees.TraversalPath](../module_reference/trees/#traversalpath) class.

## Tree

The [serializable\_trees.trees.Tree](../module_reference/trees/#tree) class.
